package rover;

public class RightCommand implements Command{
    @Override
    public void apply(Position position) {

            position.setDirection(MarsRover.DIRECTIONS.get((MarsRover.DIRECTIONS.indexOf(position.getDirection()) + 1) %
                    MarsRover.DIRECTIONS.size()));
    }
}
