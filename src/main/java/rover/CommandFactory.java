package rover;

import java.util.HashMap;
import java.util.Map;

public class CommandFactory {
    private final Map<String, Command> commands;

    private CommandFactory() {
        commands = new HashMap<>();
    }

    public void addCommand(final String name, final Command command) {
        commands.put(name, command);
    }

    public void executeCommand(Position position, String name) {
        if(commands.containsKey(name))
            commands.get(name).apply(position);
    }

    public static CommandFactory init() {
        final CommandFactory cf = new CommandFactory();

        cf.addCommand("L", new LeftCommand());
        cf.addCommand("R", new RightCommand());
        cf.addCommand("M", new MoveCommand());

        return cf;
    }

}
