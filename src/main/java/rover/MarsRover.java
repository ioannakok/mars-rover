package rover;


import java.util.ArrayList;

public class MarsRover {

    private static final CommandFactory cf = CommandFactory.init();
    public static ArrayList<Direction> DIRECTIONS = new ArrayList<Direction>() {{
        add(Direction.N);
        add(Direction.E);
        add(Direction.S);
        add(Direction.W);
    }};

    private Position position;

    public MarsRover(int startingX, int startingY, Direction direction) {
        this.position = new Position(startingX, startingY, direction);
    }

    public String run(String input) {
        String[] commands = convertInputIntoCommands(input);
        for (String command : commands) {
            cf.executeCommand(position, command);
        }
        return asString();
    }

    private String asString() {
        return position.getX() + " " + position.getY() + " " + position.getDirection();
    }

    private static String[] convertInputIntoCommands(String input) {
        String[] commandArray = input.split("(?!^)");
        return commandArray;
    }

}
