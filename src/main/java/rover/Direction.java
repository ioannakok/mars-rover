package rover;

public enum Direction {
    N(0,1),
    E(1,0),
    S(0,-1),
    W(-1,0);

    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    Direction(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
