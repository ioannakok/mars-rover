package rover;

import javafx.geometry.Pos;

public class MoveCommand implements Command {
    @Override
    public void apply(Position position) {
            position.setX(position.getX() + position.getDirection().getX());
            position.setY(position.getY() + position.getDirection().getY());

    }
}
