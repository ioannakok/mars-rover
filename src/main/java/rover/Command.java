package rover;

@FunctionalInterface
public interface Command {
    void apply(Position position);
}
