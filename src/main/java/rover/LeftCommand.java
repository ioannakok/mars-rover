package rover;

public class LeftCommand implements Command {

    @Override
    public void apply(Position position) {
            position.setDirection(MarsRover.DIRECTIONS.get((MarsRover.DIRECTIONS.indexOf(position.getDirection()) + 3) %
                    MarsRover.DIRECTIONS.size()));

    }
}
